--- 
title: "Bookdown Example"
author: "Binbin Zhao"
date: "`r Sys.Date()`"
url: http\://dashohoxha.gitlab.io/bookdown-example
description: "This is a minimal example of using bookdown to write a book by binbinzhao."

bibliography: [assets/book.bib, assets/packages.bib]
biblio-style: apalike
link-citations: yes

site: bookdown::bookdown_site
documentclass: scrbook
output:
  bookdown::gitbook:
    css: assets/style.css
    split_by: chapter            # chapter | chapter+number | section | section+number | rmd
    #number_sections: FALSE
    highlight: tango
    config:
      edit: https://gitlab.com/dashohoxha/bookdown-example/edit/master/%s
      download: ["pdf", "epub", "tex"]
      toc:
        collapse: section        # section | subsection | none
        before: |
          <li><a href="./">Bookdown Example</a></li>
      toolbar:
        position: fixed          # fixed | static
      fontsettings:
        theme: white             # white | sepia | night
        family: serif            # serif | sans
        size: 2
      sharing:
        facebook: no
        twitter: no
        all: ['facebook', 'twitter', 'linkedin', 'google']
  bookdown::pdf_book:
    #number_sections: FALSE
    #includes:
    #  in_header: assets/preamble.tex
    latex_engine: xelatex
    citation_package: natbib
    keep_tex: yes
    #fig_caption: FALSE
    highlight: tango
  bookdown::epub_book:
    #number_sections: FALSE
    #fig_caption: FALSE
---
